﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ContactWeb.Repositories.Data;
using ContactWeb.Contracts.AbstractClasses;
using ContactWeb.Contracts.Interfaces;

namespace ContactWeb.Repositories.Crud
{
    public class DbOps<Model> : IDisposable, IRepository<Model> where Model : ModelBase
    {
        private DbContext _DbContext;
        private DbSet<Model> _DbSet;

        public DbOps()
        {
            _DbContext = new ApplicationDbContext();
            _DbSet = _DbContext.Set<Model>(); 
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _DbContext.SaveChangesAsync();
        }

        public void Delete(Model t)
        {
            _DbContext.Entry(t).State = EntityState.Deleted;
        }

        public async Task<Model> FindAsync(int Id)
        {
            return await _DbSet.FindAsync(Id);
        }

        public void Add(Model t)
        {
            _DbSet.Add(t);
        }

        public IQueryable<Model> List()
        {
            return _DbSet;
        }

        public void Update(Model t)
        {
            _DbContext.Entry(t).State = EntityState.Modified;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                _DbContext.Dispose(); 
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // GC.SuppressFinalize(this);
        }

        #endregion

    }
}
