﻿
using ContactWeb.Contracts.Models;
using System.Data.Entity;


namespace ContactWeb.Repositories.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection") { }
        public DbSet<Contact> Contact { get; set; }
    }
}
