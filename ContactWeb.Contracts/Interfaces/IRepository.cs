﻿
using ContactWeb.Contracts.AbstractClasses;
using System.Threading.Tasks;

namespace ContactWeb.Contracts.Interfaces
{
    public interface IRepository<Model> where Model : ModelBase
    {
        System.Linq.IQueryable<Model> List();
        Task<Model> FindAsync(int Id);
        void Delete(Model t);
        void Add(Model t);
        void Update(Model t);
        Task<int> SaveChangesAsync();
    }

}
