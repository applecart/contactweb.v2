﻿
using ContactWeb.Contracts.AbstractClasses;
using System;
using System.ComponentModel.DataAnnotations;

namespace ContactWeb.Contracts.Models
{
    public class Contact : ModelBase
    {
        public override int ID { get; set; }
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhonePrimary { get; set; }
        public string PhoneSecondary { get; set; }
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

    }

}
