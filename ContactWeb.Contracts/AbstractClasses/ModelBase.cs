﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactWeb.Contracts.AbstractClasses
{
    public abstract class ModelBase
    {
        public abstract int ID { get; set; }

    }
}
