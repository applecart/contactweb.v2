﻿using ContactWeb.Contracts.Models;
using ContactWeb.Repositories.Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactWeb.App.Contacts
{
    public class Crud
    {
        private static DbOps<Contact> _DbOps;

        public Crud()
        {
            _DbOps = new DbOps<Contact>();
        }

        public IQueryable<Contact> GetContactsList()
        {
            return _DbOps.List();
        }

        public async Task<Contact> FindContactAsyc(int Id)
        {
            return await _DbOps.FindAsync(Id);
        }

        public void AddContact(Contact contact)
        {
            _DbOps.Add(contact);
        }

        public void DeleteContact(Contact contact)
        {
            _DbOps.Delete(contact);
        }

        public void UpdateContact(Contact contact)
        {
            _DbOps.Update(contact);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _DbOps.SaveChangesAsync(); 
        }

    }
}
