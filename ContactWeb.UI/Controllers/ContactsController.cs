﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ContactWeb.Contracts.Models;

namespace ContactWeb.Controllers
{
    public class ContactsController : Controller
    {
        protected ContactWeb.App.Contacts.Crud _Crud;
        public ContactsController()
        {
           _Crud = new ContactWeb.App.Contacts.Crud();
        }
        public ActionResult Index()
        {
            ViewData["Title"] = "List of available contacts";
            ViewBag.CrudAction = "Add";
            var contacts = _Crud.GetContactsList();   
            return View(contacts);

        }

        public ActionResult Add()
        {
            PrepFormControls();
            ViewData["Title"] = "Adding a contact";
            ViewBag.CrudAction = "Create";
            var contact = new ContactWeb.Contracts.Models.Contact();
            contact.Birthday = DateTime.Now.AddYears(-21);
            contact.UserId = Guid.NewGuid();
            return View("Edit", contact);

        }

        public async Task<ActionResult> Edit(int id)
        {
            PrepFormControls();
            ViewData["Title"] = "Editing a contact";
            ViewBag.CrudAction = "Update";
            var contact = await _Crud.FindContactAsyc(id); 
            return View(contact);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Contact contact)
        {
            ViewData["Title"] = "Adding a Service Type";
            if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", contact); }
            _Crud.AddContact(contact);
            await _Crud.SaveChangesAsync();  
            return RedirectToAction(GetIndexAction());

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(Contact contact, string updateType)
        {
            ViewData["Title"] = "Editing a contact";

            if (updateType.Equals("Delete"))
            {
                _Crud.DeleteContact(contact);
                await _Crud.SaveChangesAsync();
            }
            else
            {
                if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", contact); }
                _Crud.UpdateContact(contact);
                await _Crud.SaveChangesAsync();
            }

            return RedirectToAction(GetIndexAction());

        }

        private void PrepFormControls(object previousActionParameters = null)
        {
            ViewBag.PreviousActionParameters = previousActionParameters;
            ViewBag.States = new List<string> { "FL", "AL", "NY", "GA" };

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);

        }

    }

}
